﻿using battleship.Directions;
using System;
using System.Collections.Generic;
using System.Text;

namespace battleship
{
    /// <summary>
    /// Represents a ship in the game logic
    /// </summary>
    public class Ship
    {
        public int Id;                          // Id of the ship
        public int Size;                        // Ship size
        public int HitCount;                    // Number of hits on this ship - destroyed when Size == HitCount
        public Direction Direction;             // Orientation

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship" /> class.
        /// </summary>
        /// <param name="size">The size.</param>
        public Ship(int id, int size)
        {
            Id = id; 
            Size = size;
            HitCount = 0;
            Direction = new InvalidDirection();
        }
    }
}
