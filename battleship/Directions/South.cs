﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.Directions
{
    /// <summary>
    /// Represents the South direction for gamelogic
    /// </summary>
    /// <seealso cref="battleship.Direction" />
    public class South : Direction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="South"/> class.
        /// </summary>
        public South()
        {
            Label = "South";
            ColDelta = 0;
            RowDelta = 1;
        }
    }
}
