﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.Directions
{
    /// <summary>
    /// Represents an invalid directions state
    /// </summary>
    /// <seealso cref="InvalidDirection" />
    /// <seealso cref="InvalidDirection" />
    public class InvalidDirection : Direction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Invalid"/> class.
        /// </summary>
        public InvalidDirection()
        {
            Label = "Invalid";
        }
    }
}
