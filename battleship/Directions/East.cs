﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.Directions
{

    /// <summary>
    /// Represents the East direction for gamelogic
    /// </summary>
    /// <seealso cref="battleship.Directions.Direction" />
    public class East : Direction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="East"/> class.
        /// </summary>
        public East()
        {
            Label = "East";
            ColDelta = 1;
            RowDelta = 0;
        }
    }
}
