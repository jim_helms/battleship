﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.Directions
{
    /// <summary>
    /// Represents the North direction for gamelogic
    /// </summary>
    /// <seealso cref="battleship.Direction" />
    public class West : Direction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="West"/> class.
        /// </summary>
        public West()
        {
            Label = "West";
            ColDelta = -1;
            RowDelta = 0;
        }
    }
}
