﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace battleship.Directions
{
    /// <summary>
    /// Represents the direction from the point of a game board grid point.
    /// </summary>
    public abstract class Direction
    {
        public string Label { get; internal set; }
        public int RowDelta { get; internal set; }
        public int ColDelta { get; internal set; }

        /// <summary>
        /// Nexts the grid point.
        /// </summary>
        /// <param name="coords">The grid point.</param>
        /// <returns></returns>
        public Coords NextCoords(Coords coords)
        {
            Coords nextCoords = new Coords(coords.Col + ColDelta, coords.Row + RowDelta);

            return nextCoords;
        }

        /// <summary>
        /// Gets all directions based on valid subclass and skipping the InvalidDirection.
        /// </summary>
        /// <returns></returns>
        public static List<Direction> GetAll()
        {
            List<Direction> directions = new List<Direction>();
            foreach (Type type in Assembly.GetAssembly(typeof(Direction)).GetTypes()
                    .Where(myType => myType.Name != "InvalidDirection" && myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Direction))))
            {
                directions.Add((Direction)Activator.CreateInstance(type));
            }

            return directions;
        }


        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <param name="gameBoard">The game board.</param>
        /// <param name="coords">The coords.</param>
        /// <param name="shipSize">Size of the ship.</param>
        /// <returns>
        ///   <c>true</c> if the specified game board is valid; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValid(GameBoard gameBoard, Coords coords, int shipSize)
        {
            bool valid = false;
            Coords lastCoords = coords;
            for (int size = 0; size < shipSize - 1; size++)
            {
                valid = false;
                Coords testCoords = NextCoords(lastCoords);

                if ((testCoords.Col >= 0) && (testCoords.Col < Program.GameWidth))          // Column range check
                {
                    if ((testCoords.Row >= 0) && (testCoords.Row < Program.GameHeight))     // Row range check
                    {
                        if (gameBoard.Grid[testCoords.Col, testCoords.Row].Status != CellStatus.Ship)
                        {
                            valid = true;
                        }
                    }
                }
                lastCoords = testCoords;
            }

            return valid;
        }
    }
}
