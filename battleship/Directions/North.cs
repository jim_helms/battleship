﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.Directions
{
    /// <summary>
    /// Represents the North direction for gamelogic
    /// </summary>
    /// <seealso cref="battleship.Direction" />
    public class North : Direction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="North"/> class.
        /// </summary>
        public North()
        {
            Label = "North";
            ColDelta = 0;
            RowDelta = -1;
        }
    }
}
