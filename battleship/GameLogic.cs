﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using battleship.Directions;
using System.Reflection;
using System.Linq;

namespace battleship
{

    /// <summary>
    /// Game logic for board setup and gameplay.
    /// </summary>
    public class GameLogic
    {
        public Random RandomNumber = new Random();      // Use one instance of random number gen per session

        List<Direction> directions = Direction.GetAll();

        /// <summary>
        /// Sets up the game board and adds Ships and Players
        /// </summary>
        /// <returns>List<GameBoard></GameBoard></returns>
        public List<GameBoard> SetupGameBoards()
        {
            List<GameBoard> gameBoards = new List<GameBoard>();            

            if((Program.GameHeight > Program.MaxBoardSize) || (Program.GameWidth > Program.MaxBoardSize))
            {
                throw new Exception("Error configured game board size exceeds the possible maximum: " + Program.MaxBoardSize.ToString());
            }
            // Setup for each player
            for (int playerIndex = 0; playerIndex < Program.PlayerCount; playerIndex++)
            {
                gameBoards.Add(new GameBoard(Program.GameWidth, Program.GameHeight, playerIndex));
                // Create player ships
                for (int shipIndex = 0; shipIndex < Program.ShipCount; shipIndex++)
                {
                    int shipSize = shipIndex + (Program.ShipMinSize);
                    Ship ship = new Ship(shipIndex, shipSize);
                    PlaceShip(gameBoards[playerIndex], ship);
                    gameBoards[playerIndex].Fleet.Add(ship);
                }
            }

            return gameBoards;
        }

        /// <summary>
        /// Places the ship.
        /// </summary>
        /// <param name="gameBoard">The game board.</param>
        /// <param name="ship">The ship.</param>
        private void PlaceShip(GameBoard gameBoard, Ship ship)
        {
            bool placed = false;
            int placeCount = 0;

            // This needs to retry until both the random gridPoint and the random orientation are clear
            while (!placed)
            {
                placed = false;
                // Get a random grid point
                Coords coords = new Coords();
                coords.Col = RandomNumber.Next(0, gameBoard.MaxCols - 1);
                coords.Row = RandomNumber.Next(0, gameBoard.MaxRows - 1);

                // Check gridPoint - status must be empty or ship
                if (gameBoard.Grid[coords.Col, coords.Row].Status != CellStatus.Ship)
                {
                    placed = true;
                }

                // If orientation randomizer cannot place ship it will return ShipOrientation.Invalid
                if (RandomizeShipDirection(gameBoard, coords, ship).Label != "Invalid")
                {
                    placed = true;
                }
                else
                {
                    // Selected point cannot fit ship try again
                    placed = false;
                }

                // If still placeable then update the GameBoard
                if (placed)
                {
                    Debug.Print("New ship @ {0},{1} - {2} size {3}{4}", coords.Col + 1, coords.Row + 1, ship.Direction.Label, ship.Size, Environment.NewLine);

                    GridPoint gridPoint;
                    Coords shipCoords = coords;

                    for (int i = 0; i < ship.Size; i++)     // Skip the first one
                    {
                        gridPoint = gameBoard.Grid[shipCoords.Col, shipCoords.Row];
                        gridPoint.Status = CellStatus.Ship;
                        gridPoint.ShipId = ship.Id;
                        Debug.Print(" - {0} @ {1},{2}{3}", gameBoard.Grid[shipCoords.Col, shipCoords.Row].Status, shipCoords.Col + 1, shipCoords.Row + 1, Environment.NewLine);

                        shipCoords = ship.Direction.NextCoords(shipCoords);

                    }
                }
                else
                    Debug.Print("Attempt {0} @ {1},{2} - {3} size {4}{5}", placeCount++, coords.Col + 1, coords.Row + 1, ship.Direction.Label, ship.Size, Environment.NewLine);
            }
        }

        /// <summary>
        /// Randomizes the ship orientation.
        /// </summary>
        /// <param name="gameBoard">The game board.</param>
        /// <param name="coords">The coords.</param>
        /// <param name="ship">The ship.</param>
        /// <returns>
        /// Direction
        /// </returns>
        private Direction RandomizeShipDirection(GameBoard gameBoard, Coords coords, Ship ship)
        {
            Direction direction = new InvalidDirection();

            List<Direction> validDirections = GetValidDirections(gameBoard, coords, ship);

            // If we have valid orientations left for this x,y pos we use it otherwise mark invalid
            if (validDirections.Count > 0)
            {
                direction = validDirections[RandomNumber.Next(0, validDirections.Count - 1)];
                ship.Direction = direction;
            }

            return direction;
        }

        /// <summary>
        /// Creates a random valid input string.
        /// </summary>
        /// <returns></returns>
        public string RandomizeInputString(bool badFormat = false)
        {
            string input = string.Empty;
            if (badFormat)
            {
                // Generate a known bad coord letter and a random number then select them randomly to make up the input
                string badNumber = RandomNumber.Next(-999, 999).ToString();
                string badLetter = Convert.ToChar(RandomNumber.Next(26, 50) + Program.UnicodeColumnOffset).ToString();
                List<String> badInputs = new List<string>() { badLetter, badNumber };
                int index1 = RandomNumber.Next(0, 1);
                int index2 = index1 == 0 ? 1 : 0;
                input = badInputs[index1] + badInputs[index2];

            }
            else
                input = Convert.ToChar(RandomNumber.Next(0, Program.GameWidth) + Program.UnicodeColumnOffset) + RandomNumber.Next(1, Program.GameHeight + 1).ToString();

            return input;
        }

        /// <summary>
        /// Gets the valid directions.
        /// </summary>
        /// <param name="gameBoard">The game board.</param>
        /// <param name="coords">The coords.</param>
        /// <param name="ship">The ship.</param>
        /// <returns></returns>
        private List<Direction> GetValidDirections(GameBoard gameBoard, Coords coords, Ship ship)
        {
            // This list will be culled as validation occurs
            List<Direction> validDirections = new List<Direction>();
            foreach (Direction direction in directions)
                validDirections.Add(direction);

            foreach (Direction direction in directions)
            {
                if (!direction.IsValid(gameBoard, coords, ship.Size))
                    validDirections.Remove(direction);
            }

            return validDirections;
        }
    }
}
