﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship
{
    /// <summary>
    /// Struct to ease passing coordinates
    /// </summary>
    public struct Coords
    {
        /// <summary>
        /// The coordinate's col and row
        /// </summary>
        public int Col, Row;

        /// <summary>
        /// Initializes a new instance of the <see cref="CoOrds"/> struct.
        /// </summary>
        /// <param name="col">The col.</param>
        /// <param name="row">The row.</param>
        public Coords(int col, int row)
        {
            Col = col;
            Row = row;
        }
    }
}
