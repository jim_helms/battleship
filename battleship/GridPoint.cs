﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship
{
    /// <summary>
    /// Represents a grid point on the gameboard
    /// </summary>
    public class GridPoint
    {
        /// <summary>
        /// Used to map CellStatus Enum to gameboard marker
        /// </summary>
        private static readonly Dictionary<CellStatus, string> statusString = new Dictionary<CellStatus, string>()
            {
                { CellStatus.Empty, "-" },
                { CellStatus.Hit, "X" },
                { CellStatus.Miss, "*" },
                { CellStatus.Ship, "S" },
            };

        /// <summary>
        /// The status of the gridPoint
        /// </summary>
        public CellStatus Status;       // Status of point
        public int ShipId = -1;         // Id of ship at point

        /// <summary>
        /// Initializes a new instance of the <see cref="GridPoint"/> class.
        /// </summary>
        public GridPoint()
        {
            Status = CellStatus.Empty;
        }

        /// <summary>
        /// Maps the Status to it's gameBoard marker
        /// </summary>
        /// <returns></returns>
        public string StatusMarker()
        {
            return StatusMarker(Status);
        }

        /// <summary>
        /// Statuses the marker.
        /// </summary>
        /// <param name="cellStatus">The cell status.</param>
        /// <returns></returns>
        public static string StatusMarker(CellStatus cellStatus)
        {
            string statusMarker = statusString[cellStatus];
            return statusMarker;
        }
    }
}
