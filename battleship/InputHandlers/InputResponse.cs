﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.InputHandlers

{
    /// <summary>
    /// Represents the state of a user response.  Controls game flow.
    /// </summary>
    public class InputResponse
    {
        public bool Exit = false;
        public bool Cheat = false;
        public bool Auto = false;
        public bool ControlResponse = false;
        public bool ColValid = false;
        public bool RowValid = false;
        public Coords Coords = new Coords(0, 0);
        public bool Valid { get { return (ColValid && RowValid);  } }
        public bool Error = false;
        public string ErrMessage = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="InputResponse"/> class.
        /// </summary>
        /// <param name="exit">if set to <c>true</c> [exit].</param>
        /// <param name="cheat">if set to <c>true</c> [cheat].</param>
        /// <param name="auto">if set to <c>true</c> [automatic].</param>
        /// <param name="controlResponse">if set to <c>true</c> [control response].</param>
        /// <param name="colValid">if set to <c>true</c> [col valid].</param>
        /// <param name="rowValid">if set to <c>true</c> [row valid].</param>
        /// <param name="error">if set to <c>true</c> [error].</param>
        /// <param name="coords">The coords.</param>
        /// <param name="errMessage">The error message.</param>
        public InputResponse(bool exit, bool cheat, bool auto, bool controlResponse, bool colValid, bool rowValid, bool error, Coords coords, string errMessage)
        {
            Exit = exit;
            Cheat = cheat;
            Auto = auto;
            ControlResponse = controlResponse;
            ColValid = colValid;
            RowValid = rowValid;
            Coords = coords;
            Error = error;
            ErrMessage = errMessage;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InputResponse"/> class.
        /// </summary>
        public InputResponse()
        {
        }
    }
}
