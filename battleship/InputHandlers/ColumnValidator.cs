﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.InputHandlers
{
    /// <summary>
    /// Validates that the input contains a valid column.
    /// </summary>
    /// <seealso cref="battleship.InputHandlers.InputHandler" />
    public class ColumnValidator : InputHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnValidator"/> class.
        /// </summary>
        public ColumnValidator()
        {
            Type = HandlerType.Validation;
        }

        /// <summary>
        /// Processes the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="actionResponse">The action response.</param>
        /// <returns></returns>
        public override bool Process(string input, InputResponse actionResponse)
        {
            bool error = base.Process(input, actionResponse);       // Perfom base null validation
            if (!error)
            {
                try
                {
                    // Check valid form and range
                    if (Char.IsLetter(base.Input[0]))
                    {
                        int testCol = base.Input[0] - Program.UnicodeColumnOffset;

                        // Range check on col
                        if ((testCol < 0) || (testCol >= Program.GameWidth))
                        {
                            error = true;
                            actionResponse.ColValid = false;
                            actionResponse.Error = true;
                            actionResponse.ErrMessage = string.Format(Program.ErrorFormat, "Column coordinate out of range!");
                        }
                        else
                        {
                            actionResponse.ColValid = true;
                            actionResponse.Coords.Col = testCol;
                        }
                    }
                    else
                    {
                        error = true;
                        actionResponse.ColValid = false;
                        actionResponse.Error = true;
                        actionResponse.ErrMessage = string.Format(Program.ErrorFormat, "Column coordinate must be a letter!");
                    }

                }
                catch (Exception ex)
                {
                    error = true;
                    actionResponse.ColValid = false;
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = string.Format(Program.ErrorFormat, string.Format("Error in ColumnValidator: {0}", ex.Message));
                }
            }
            return error;
        }
    }
}
