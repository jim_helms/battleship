﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.InputHandlers
{
    /// <summary>
    /// Validates that the input contains a valid Row
    /// </summary>
    /// <seealso cref="battleship.InputHandlers.InputHandler" />
    public class RowValidator : InputHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RowValidator"/> class.
        /// </summary>
        public RowValidator()
        {
            Type = HandlerType.Validation;
        }

        /// <summary>
        /// Processes the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="actionResponse">The action response.</param>
        /// <returns></returns>
        public override bool Process(string input, InputResponse actionResponse)
        {
            bool error = base.Process(input, actionResponse);       // Perfom base null validation
            if (!error)
            {
                try
                {
                    // Check valid form and range
                    // Validate row number
                    int testRow = 0;
                    if (!int.TryParse(base.Input.Substring(1), out testRow))
                    {
                        error = true;
                        actionResponse.RowValid = false;
                        actionResponse.Error = true;
                        actionResponse.ErrMessage = string.Format(Program.ErrorFormat, "Row coordinate must be a valid integer!");
                    }
                    else
                    {
                        // Range check on row
                        testRow--;              // Convert to zero based for internal logic
                        if ((testRow < 0) || (testRow >= Program.GameHeight))
                        {
                            error = true;
                            actionResponse.RowValid = false;
                            actionResponse.Error = true;
                            actionResponse.ErrMessage = string.Format(Program.ErrorFormat, "Row out of range!");
                        }
                        else
                        {
                            actionResponse.RowValid = true;
                            actionResponse.Coords.Row = testRow;
                        }
                    }
                }
                catch (Exception ex)
                {
                    error = true;
                    actionResponse.RowValid = false;
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = string.Format(Program.ErrorFormat, string.Format("Error in RowValidator: {0}", ex.Message));
                }
            }
            return error;
        }
    }
}
