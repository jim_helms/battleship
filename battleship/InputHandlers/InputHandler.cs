﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace battleship.InputHandlers
{
    /// <summary>
    /// The InputHandler base class.  InputHandlers take the input string and modify the inputResponse state object 
    /// depending on each handler's rules.  Each handler calls the base to do any common
    /// processing - e.g. the example I used was a simple null check and normalizing the 
    /// input to uppercase.
    /// </summary>
    public abstract class InputHandler
    {
        public string Input { get; private set; }
        public HandlerType Type;

        /// <summary>
        /// Initializes a new instance of the <see cref="InputHandler"/> class.
        /// </summary>
        public InputHandler()
        {
            Type = HandlerType.Action;
        }

        /// <summary>
        /// Gets all input handlers based on valid subclass and skipping the InvalidDirection.
        /// </summary>
        /// <returns></returns>
        public static List<InputHandler> GetAll()
        {
            List<InputHandler> inputHandlers = new List<InputHandler>();
            foreach (Type type in Assembly.GetAssembly(typeof(InputHandler)).GetTypes()
                .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(InputHandler))))
            {
                inputHandlers.Add((InputHandler)Activator.CreateInstance(type));
            }

            return inputHandlers;
        }

        public virtual bool Process(string input, InputResponse actionResponse)
        {
            bool error = false;
            try
            {
                Input = input.ToUpper();
                if (string.IsNullOrEmpty(input))
                {
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = "Error in InputHandler: Input cannot be empty!";
                }
            }
            catch (Exception ex)
            {
                error = true;
                actionResponse.Error = true;
                actionResponse.ErrMessage = string.Format(Program.ErrorFormat, string.Format("Error in InputHandler: {0}", ex.Message));
            }

            return error;
        }
    } 
}
