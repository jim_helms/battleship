﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.InputHandlers
{
    /// <summary>
    /// Handler for cheat keyword
    /// </summary>
    /// <seealso cref="battleship.InputHandlers.InputHandler" />
    public class CheatHandler : InputHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CheatHandler" /> class.
        /// </summary>
        public CheatHandler()
        {
            Type = HandlerType.Action;
        }

        /// <summary>
        /// Processes the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="actionResponse">The action response.</param>
        /// <returns></returns>
        public override bool Process(string input, InputResponse actionResponse)
        {
            bool error = base.Process(input, actionResponse);       // Perfom base null validation
            if (!error)
            {                
                try
                {
                    if (base.Input == Program.CheatKeyword)
                    {
                        actionResponse.ControlResponse = true;
                        actionResponse.Cheat = !actionResponse.Cheat;
                    }
                }
                catch (Exception ex)
                {
                    error = true;
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = string.Format(Program.ErrorFormat, string.Format("Error in CheatHandler: {0}", ex.Message));
                }
            }
            return error;
        }
    }
}
