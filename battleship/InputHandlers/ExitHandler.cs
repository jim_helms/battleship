﻿using System;
using System.Collections.Generic;
using System.Text;

namespace battleship.InputHandlers
{
    /// <summary>
    /// Handles the exit keyword
    /// </summary>
    /// <seealso cref="battleship.InputHandlers.InputHandler" />
    public class ExitHandler : InputHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExitHandler"/> class.
        /// </summary>
        public ExitHandler()
        {
            Type = HandlerType.Action;
        }

        /// <summary>
        /// Processes the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="actionResponse">The action response.</param>
        /// <returns></returns>
        public override bool Process(string input, InputResponse actionResponse)
        {
            bool error = base.Process(input, actionResponse);       // Perfom base null validation
            if (!error)
            {                
                try
                {
                    if (base.Input == Program.ExitKeyword)
                    {
                        actionResponse.ControlResponse = true;
                        actionResponse.Exit = true;
                    }
                }
                catch (Exception ex)
                {
                    error = true;
                    actionResponse.Error = true;
                    actionResponse.ErrMessage = string.Format(Program.ErrorFormat, string.Format("Error in ExitHandler: {0}", ex.Message));
                }
            }
            return error;
        }
    }
}
