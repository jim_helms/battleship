﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using battleship.InputHandlers;
using System.Reflection;
using System.Linq;
using System.Threading;

/// <summary>
/// Battleship game prototype.
/// </summary>
namespace battleship
{
    /// <summary>
    /// Indicates the status of a particular GridPoint on the game board
    /// </summary>
    public enum CellStatus
    {
        Empty,
        Ship,
        Miss,
        Hit,
    }

    /// <summary>
    /// Indicates the type of an input handler
    /// </summary>
    public enum HandlerType
    {
        Action,
        Validation,
    }

    /// <summary>
    /// Main program entry point
    /// </summary>
    public class Program
    {
        // For prototype use const as placeholder for possible future configurable options
        public const int MaxBoardSize = 26;                             // Added limit in case of incorrect GameWidth/Height setting
        public const int GameWidth = 10;                                // GameBoard width
        public const int GameHeight = 10;                               // GameBoard Height
        public const int MarginLeft = 2;                                // GameBoard left margin
        public const int PlayerCount = 2;                               // Number of players
        public const int ShipMinSize = 2;                               // Smallest ship size
        public const int ShipCount = 4;                                 // Max number of ships - assumption per reqs that each ship increases in size by one
        public const int UnicodeColumnOffset = 65;                      // Column names start at 'A' - this is Unicode offset
        public const string ExitKeyword = "EXIT";                       // Termination command
        public const string CheatKeyword = "CHEAT";                     // Cheat command - toggles disply of Ships on map
        public const string AutoKeyword = "AUTO";                       // Auto command - switches to auto completion mode

        // Game control text blobs
        private const string shipDestroyText = " - Ship destroyed!";    
        private static readonly string pauseText = new string(' ', MarginLeft) + "Press any key to continue...";
        private static readonly string promptFormat = new string(' ', MarginLeft) + "Turn {0} - Enter Grid Point (col + row) to attack or type 'Exit' to leave.";
        private static readonly string inputFormat = new string(' ', MarginLeft) + "Player {0}:";
        private static readonly string resultFormat = new string(' ', MarginLeft) + "Last move: Player {0} {1} {2}";
        private static readonly string victoryFormat = new string(' ', MarginLeft) + "Player {0} WINS!!!!!";

        // Error format
        public static readonly string ErrorFormat = new string(' ', MarginLeft) + "Error: {0}" + Environment.NewLine + pauseText;

        /// <summary>
        /// Program entry point
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            GameLoop(args);
        }

        /// <summary>
        /// Main console loop.  Handles user input and orchestrates display
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void GameLoop(string[] args)
        {
            try               // Just the most basic error handler
            {
                List<InputHandler> inputHandlers = InputHandler.GetAll();

                GameLogic gameLogic = new GameLogic();
                List<GameBoard> gameBoards = gameLogic.SetupGameBoards();

                // Loop until a player uses the exit keyword or until the game is complete, alternating players. 
                // Gameboards for both players are always shown and there is a cheat mode which makes the ships visible for debugging.
                int playerIndex = 0;                // Tracks current player
                int gameTurn = 0;                   // Tracks current turn
                string turnResult = string.Empty;   // Used to display last turns result
                InputResponse inputResponse = new InputResponse(false, false, false, false, false, false, false, new Coords(0, 0), string.Empty);

                while (!inputResponse.Exit)
                {                    
                    DisplayGameBoards(gameBoards, inputResponse.Cheat);      // Display game boards
                    Console.WriteLine(promptFormat, gameTurn + 1);           // Display user prompt
                    Console.WriteLine(turnResult);                           // Display last turn's result
                    if (!inputResponse.Auto)                                 // Small UI delay for user to see opponent auto response unless in auto mode
                        Thread.Sleep(500);                      
                    Console.Write(inputFormat, playerIndex + 1);             // Display player prompt

                    
                    string input = string.Empty;
                    if (inputResponse.Auto || playerIndex == 1)              // Auto play check - Will automatically generate the firing coords
                    {
                        input = gameLogic.RandomizeInputString();
                        Console.Write(input);
                    }
                    else
                        input = Console.ReadLine();                          // Wait for user input

                    // Validate and process input - first reset state flags
                    inputResponse.ErrMessage = string.Empty;
                    inputResponse.Error = false;
                    inputResponse.ColValid = false;
                    inputResponse.RowValid = false;
                    inputResponse.ControlResponse = false;
                    inputResponse.Coords = new Coords(0, 0);

                    foreach (InputHandler inputHandler in inputHandlers)     // Run each handler to validate the input string
                    {
                        bool processFailed = inputHandler.Process(input, inputResponse);        // Run handler and check for failure
                        if (processFailed)
                        {
                            Console.WriteLine(inputResponse.ErrMessage);
                            Console.ReadKey();
                            break;
                        }
                        else
                            if (inputResponse.ControlResponse)
                                break;
                    }
                    
                    if (inputResponse.Valid)                                 // Input validated
                    {
                        CellStatus cellStatus = gameBoards[playerIndex].AttackTarget(inputResponse.Coords);
                        bool gameOver = false;
                        string shipStatus = string.Empty;
                        if (cellStatus == CellStatus.Hit)                    // Check for ship destruction
                        {
                            if (gameBoards[playerIndex].ShipDestroyed(inputResponse.Coords))
                                shipStatus = shipDestroyText;
                            
                            gameOver = gameBoards[playerIndex].FleetDestroyed();        // Check for game win
                        }
                        
                        if (gameOver)                                       // If game is over we can exit
                        {
                            DisplayGameBoards(gameBoards, true);            // Show final gameboards
                            Console.WriteLine(victoryFormat, playerIndex + 1);
                            Console.WriteLine(pauseText);
                            Console.ReadKey();
                            inputResponse.Exit = true;
                        }

                        turnResult = string.Format(resultFormat, playerIndex + 1, cellStatus, shipStatus);      // Set visual result
                        playerIndex++;

                        if (playerIndex >= PlayerCount)                     // Check for turn end
                        {
                            playerIndex = 0;
                            gameTurn++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ErrorFormat, "Unexpected error: " + ex.Message);
                Console.ReadKey();
            }
} // Gameloop        

        /// <summary>
        /// Displays the game boards.
        /// </summary>
        /// <param name="gameBoards">The game boards.</param>
        /// <param name="showAllShips">if set to <c>true</c> [show all ships].</param>
        private static void DisplayGameBoards(List<GameBoard> gameBoards, bool showAllShips)
        {
            Console.Clear();
            gameBoards[0].WriteHeader();
            foreach (GameBoard gameBoard in gameBoards)
            {
                gameBoard.Display(showAllShips);
            }
            gameBoards[0].WriteFooter();
        }

    } // Program
} // battleship
