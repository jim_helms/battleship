﻿ using System;
using System.Collections.Generic;
using System.Text;

namespace battleship
{
    /// <summary>
    /// Game UI and board metadata.  Assumes a game board grid with 0,0 in the top left corner.
    /// </summary>
    public class GameBoard
    {
        public int MaxCols;                     // Grid width
        public int MaxRows;                     // Grid height
        public int PlayerId;                    // Player ID
        public GridPoint[,] Grid;               // Array of grids for players
        public List<Ship> Fleet;                // List of ships on the board

        /// <summary>
        /// Initializes a new instance of the <see cref="GameBoard"/> class.
        /// </summary>
        /// <param name="Width">The width.</param>
        /// <param name="Height">The height.</param>
        /// <param name="NumberOfPlayers">The number of players.</param>
        public GameBoard(int Width, int Height, int playerId)
        {
            MaxCols = Width;
            MaxRows = Height;
            PlayerId = playerId;
            Fleet = new List<Ship>();
            Grid = new GridPoint[MaxCols, MaxRows];
            for (int col = 0; col < MaxCols; col++)
            {
                for (int row = 0; row < MaxRows; row++)
                {
                    Grid[col, row] = new GridPoint();
                }
            }
        }

        /// <summary>
        /// Attacks the target.
        /// </summary>
        /// <param name="fireCoords">The fire coords.</param>
        /// <returns></returns>
        public CellStatus AttackTarget(Coords fireCoords)
        {
            CellStatus cellStatus = Grid[fireCoords.Col, fireCoords.Row].Status;
            if (!(cellStatus == CellStatus.Hit))
            {
                if (Grid[fireCoords.Col, fireCoords.Row].Status == CellStatus.Ship)
                {
                    cellStatus = CellStatus.Hit;
                    Fleet[Grid[fireCoords.Col, fireCoords.Row].ShipId].HitCount++;
                }
                else
                {
                    if (Grid[fireCoords.Col, fireCoords.Row].Status == CellStatus.Empty)
                        cellStatus = CellStatus.Miss;
                }
                Grid[fireCoords.Col, fireCoords.Row].Status = cellStatus;
            }
            return cellStatus;
        }

        /// <summary>
        /// Determines if ship is destroyed
        /// </summary>
        /// <param name="fireCoords">The fire coords.</param>
        /// <returns></returns>
        public bool ShipDestroyed(Coords fireCoords)
        {
            bool destroyed = false;

            Ship ship = Fleet[Grid[fireCoords.Col, fireCoords.Row].ShipId];
            if (ship.HitCount == ship.Size)
                destroyed = true;

            return destroyed;
        }

        /// <summary>
        /// Fleets the destroyed.
        /// </summary>
        /// <returns></returns>
        public bool FleetDestroyed()
        {
            bool destroyed = true;
            foreach (Ship ship in Fleet)
            {
                if (ship.HitCount < ship.Size)
                {
                    destroyed = false;
                    break;
                }
            }
            return destroyed;
        }

        // Layout and formatting

        /// <summary>
        /// Writes the header.
        /// </summary>
        public void WriteHeader()
        {
            Console.WriteLine(new string(' ', Program.MarginLeft) + "-- B A T T L E S H I P --");
        }

        /// <summary>
        /// Writes the footer.
        /// </summary>
        public void WriteFooter()
        {
            string legend = new string(' ', Program.MarginLeft) + "Legend: ";
            foreach (CellStatus cellStatus in Enum.GetValues(typeof(CellStatus)))
            {
                legend += GridPoint.StatusMarker(cellStatus) + "=" + Enum.GetName(typeof(CellStatus), cellStatus) + " ";
            }
            string line = new string(' ', Program.MarginLeft) + new string('-', legend.Length);
            Console.WriteLine(line);
            Console.WriteLine(legend);
            Console.WriteLine(line);
        }

        /// <summary>
        /// Displays this instance.
        /// </summary>
        public void Display(bool showAllShips)
        {
            Console.WriteLine("{0}   Player {1}", new string(' ', Program.MarginLeft), PlayerId + 1);
            Console.Write(new string(' ', Program.MarginLeft) + "   ");
            for (int col = 0; col < MaxCols; col++)
                Console.Write(Convert.ToChar(Program.UnicodeColumnOffset + col));
            Console.WriteLine();

            for (int row = 0; row < MaxRows; row++)
            {
                Console.Write(new string(' ', Program.MarginLeft) + "{0,2}-", row + 1);
                for (int col = 0; col < MaxCols; col++)
                {
                    string statusMarker = Grid[col, row].StatusMarker();

                    if (!showAllShips)
                    { 
                        // Hide ships if not in cheat mode
                        if (Grid[col, row].Status == CellStatus.Ship)
                        {
                            statusMarker = GridPoint.StatusMarker(CellStatus.Empty);
                        }
                    }
                    Console.Write(statusMarker);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Gets the name of the cell.
        /// </summary>
        /// <param name="col">The x.</param>
        /// <param name="row">The y.</param>
        /// <returns></returns>
        public string GetCellName(int col, int row)
        {
            char colLabel = Convert.ToChar(Program.UnicodeColumnOffset + col);
            string name = string.Format("{0}-{1}", colLabel, row);
            return name;
        }
    }
}
