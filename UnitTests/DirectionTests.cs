using Microsoft.VisualStudio.TestTools.UnitTesting;
using battleship.Directions;
using System.Collections.Generic;
using battleship;

namespace UnitTests
{
    [TestClass]
    public class DirectionTests
    {
        [TestMethod]
        public void NextCoordsTest()
        {
            Direction north = new North();
            Direction east = new East();
            Direction south = new South();
            Direction west = new West();

            Coords testCoords = new Coords(4, 4),
                    validNorth = new Coords(4, 3),
                    validEast = new Coords(5, 4),
                    validSouth = new Coords(4, 5),
                    validWest = new Coords(3, 4);


            Assert.AreEqual(validNorth, north.NextCoords(testCoords));
            Assert.AreEqual(validEast, east.NextCoords(testCoords));
            Assert.AreEqual(validSouth, south.NextCoords(testCoords));
            Assert.AreEqual(validWest, west.NextCoords(testCoords));

        }

        [TestMethod]
        public void GetAllDirectionsTest()
        {
            List<Direction> directions = Direction.GetAll();

            int validCount = 4;
            Assert.AreEqual(validCount, directions.Count);

            foreach(Direction direction in directions)
            {
                string typeName = direction.GetType().Name;
                Assert.AreEqual(typeName, direction.Label);
            }

        }

        [TestMethod]
        public void IsValidTest()
        {
            // Top left - South East
            // Top right - South West
            // Bot left - North, East
            // Bot right - North, West
            // Center - All

            // All grid points?
        }
    }
}
