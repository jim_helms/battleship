using Microsoft.VisualStudio.TestTools.UnitTesting;
using battleship.InputHandlers;
using System.Collections.Generic;
using battleship;

namespace UnitTests
{
    [TestClass]
    public class InputHandlerTests

    {
        /// <summary>
        /// Test the GetAll function
        /// </summary>
        public void GetAllTest()
        {
            int validCount = 5;
            List<InputHandler> inputHandlers = InputHandler.GetAll();

            Assert.AreEqual(validCount, inputHandlers.Count);
        }

        /// <summary>
        /// Process success test - validates logic
        /// </summary>
        [TestMethod]
        public void ProcessValidInputTest()
        {
            GameLogic gameLogic = new GameLogic();
            List<InputHandler> inputHandlers = InputHandler.GetAll();
            InputResponse inputResponse = new InputResponse();
            List<string> validInputs = new List<string>() { Program.AutoKeyword, Program.CheatKeyword, Program.ExitKeyword, gameLogic.RandomizeInputString() };
            foreach (InputHandler inputHandler in inputHandlers)
            {
                foreach (string validInput in validInputs)
                {
                    bool processFailed = inputHandler.Process(validInput, inputResponse);        
                    Assert.IsFalse(processFailed && !inputResponse.ControlResponse);
                }

                for (int i = 0; i <  10000; i++)
                {
                    bool processFailed = inputHandler.Process(gameLogic.RandomizeInputString(), inputResponse) && !inputResponse.ControlResponse;        
                    Assert.IsFalse(processFailed);
                }
            }

        }

        [TestMethod]
        public void ProcessBadInputTest()
        {
            GameLogic gameLogic = new GameLogic();
            List<InputHandler> inputHandlers = InputHandler.GetAll();
            InputResponse inputResponse = new InputResponse();
            foreach (InputHandler inputHandler in inputHandlers)
            {
                for (int i = 0; i < 10000; i++)
                {
                    bool processFailed = false;
                    if (inputHandler.Type == HandlerType.Validation)
                    {
                        string badInput = gameLogic.RandomizeInputString(true);
                        processFailed = inputHandler.Process(badInput, inputResponse);
                        processFailed = processFailed || inputResponse.Error;
                        Assert.IsTrue(processFailed);
                    }
                    else
                        // Action type input handlers can consume any text w/o error so looking for unhandlad
                        Assert.IsFalse(processFailed);      
                }
            }
        }
    }
}
